#ifndef DOOMDEFENSEDIALOG_H
#define DOOMDEFENSEDIALOG_H

#include <QDialog>

namespace Ui {
class DoomDefenseDialog;
}

class DoomDefenseDialog : public QDialog
{
    Q_OBJECT

public:
    explicit DoomDefenseDialog(QString mainText, QString infText, QWidget *parent = 0);

    void setAddedBtnText(QString text);

    ~DoomDefenseDialog();

private slots:
    void on_btn_added_clicked();

private:
    Ui::DoomDefenseDialog *ui;
};

#endif // DOOMDEFENSEDIALOG_H
