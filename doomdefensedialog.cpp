#include "doomdefensedialog.h"
#include "ui_doomdefensedialog.h"

DoomDefenseDialog::DoomDefenseDialog(QString mainText, QString infText, QWidget *parent) :
    QDialog(parent),
    ui(new Ui::DoomDefenseDialog)
{
    ui->setupUi(this);

    ui->lbl_informativeText->setText(infText);
    ui->lbl_mainText->setText(mainText);
}

DoomDefenseDialog::~DoomDefenseDialog()
{
    delete ui;
}

void DoomDefenseDialog::on_btn_added_clicked()
{
    this->close();
}

void DoomDefenseDialog::setAddedBtnText(QString text)
{
    this->ui->btn_added->setText(text);
}
