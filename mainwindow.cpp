#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    slm_rooms = new QStringListModel();
    slm_selected_rooms = new QStringListModel();

    ui->lv_all_rooms->setModel(slm_rooms);
    ui->lv_selected_rooms->setModel(slm_selected_rooms);

    this->init_signals();

    const QString s_org_name = "Okeanpribor";
    const QString s_app_name = "EntryLevels";
    this->p_settings = new QSettings(s_org_name, s_app_name);

    QString s_prev_filename = this->p_settings->value("/Data/PreviousFile", "No file").toString();

    if (s_prev_filename != "No File") {
        this->load_data(s_prev_filename);
    }
}

MainWindow::~MainWindow()
{
    delete ui;

    delete slm_rooms;
    delete slm_selected_rooms;
}

void MainWindow::init_signals()
{
    QObject::connect(ui->btn_choose_file, SIGNAL(clicked()),
                     this, SLOT(choose_file()));

    ui->sb_predefined_level->installEventFilter(this);
}

void MainWindow::choose_file()
{
    QString file_name = QFileDialog::getOpenFileName(this, "Открыть файл",
                                                     "",
                                                     "*.xlsx");
    if (file_name.isEmpty()) {
        qDebug() << "No filename specified";
    }
    else {
        ui->le_filename->setText(file_name);
        load_data(file_name);
    }

    return;
}

void MainWindow::load_data(QString s_file_name)
{
    QFileInfo fi = QFileInfo(s_file_name);

    if (!fi.isFile()) {
        return;
    } else {
        ui->le_filename->setText(s_file_name);
    }


    entry_levels = new ExtXlsxDocument(s_file_name);

    this->p_settings->setValue("/Data/PreviousFile", s_file_name);

    sl_rooms = QStringList();
    slm_rooms->setStringList(sl_rooms);

    sl_selected_rooms = QStringList();
    slm_selected_rooms->setStringList(sl_selected_rooms);

    l_entry_levels = QList<entry_level>();

    this->update_levels();

    // Считываем названия помещений
    QString s_room;
    QXlsx::Cell *current_cell;

    for (int col = 2; ; ++col) {
        current_cell = entry_levels->cellAt(1, col);

        if (!current_cell) {
            break;
        }

        s_room = current_cell->value().toString().trimmed();

        if (s_room.isEmpty()) {
            break;
        }

        sl_rooms.append(s_room);
    }
    sl_rooms.sort();
    slm_rooms->setStringList(sl_rooms);

    uint f_level;
    QString s_value;

//    Считываем номера уровней доступа, и помещения, куда есть доступ на этом уровне
    for (int row = 2; row <= entry_levels->rowsCount; ++row) {
        current_cell = entry_levels->cellAt(row, 1);

        if (!current_cell) {
            break;
        }

        f_level = current_cell->value().toUInt();

        if (f_level == 0) {
            break;
        }

        entry_level el_level;
        el_level.n_level = f_level;

        for (int col = 2; col <= entry_levels->colsCount; ++col) {
            current_cell = entry_levels->cellAt(row, col);

            if (!current_cell) {
                continue;
            }

            s_value = current_cell->value().toString().trimmed();

            if (!s_value.isEmpty()) {
                el_level.sl_accessed_rooms.append(entry_levels->cellAt(1, col)->value().toString());
                el_level.sl_accessed_rooms.sort();
            }
        }

        int ind = l_entry_levels.indexOf(el_level);

        if (ind != -1) {

            entry_level el_original = l_entry_levels.at(ind);
            QMessageBox msgBox(QMessageBox::Information,
                               "Дублирующиеся уровни доступа",
                               "Найден дубликат уровня доступа №" + QString::number(el_original.n_level)
                                                          + " под номером №" + QString::number(el_level.n_level) + "\n"
                                                          + "Удалить уровень доступа №" + QString::number(el_level.n_level) + "?",
                               QMessageBox::Cancel | QMessageBox::Ok);

            msgBox.setWindowModality(Qt::WindowModal);
            int ret = msgBox.exec();

            switch (ret) {
            case QMessageBox::Ok:
            {
                entry_levels->removeRow(row);

                for (int n_mov_row = row + 1; n_mov_row <= entry_levels->rowsCount; ++n_mov_row) {
                    entry_levels->moveRow(n_mov_row, n_mov_row - 1);
                }

//                Так как мы подвинули все строки наверх, то теперь надо, чтобы цикл начал следующую итерацию
                --row;
                entry_levels->save();

                DoomDefenseDialog dddBox("<b>Удалён уровень доступа №" + QString::number(el_level.n_level) + ". Обновите информацию в АБД!</b>",
                                         "",
                                         this);

                dddBox.setAddedBtnText(QString("Я обновил!"));

                dddBox.setWindowModality(Qt::WindowModal);
                dddBox.exec();
            }
                break;
            case QMessageBox::Cancel:
                l_entry_levels.append(el_level);
                break;
            default:
                l_entry_levels.append(el_level);
                break;
            }
        } else {
            l_entry_levels.append(el_level);
        }
    }

    QSpinBox *sb_level = ui->sb_predefined_level;

    int n_max_level = 0;

    int n_levels_count = l_entry_levels.length();

    if (n_levels_count > 0) {
        entry_level el_max = l_entry_levels.at(0);
        n_max_level = el_max.n_level;

        foreach (entry_level el_cur, l_entry_levels) {
            if (el_cur > el_max)
                n_max_level = el_cur.n_level;
        }

        sb_level->setMaximum(n_max_level);
        sb_level->setEnabled(true);
    } else {
        sb_level->setValue(1);
        sb_level->setEnabled(false);
    }

    return;
}

void MainWindow::update_levels()
{
//    В этот список заносятся возможные уровни доступа
    QList<entry_level> l_possible_levels;
//    Строка с точным уровнем доступа
    QString s_calc_level;
//    Строка с возможными уровнями доступа
    QString s_poss_levels;

//    Если выбранных комнат нет, то просто затираем текущий текст
    if (slm_selected_rooms->stringList().length() == 0) {
        ui->lbl_calculated_level->setText("");
        ui->lbl_possible_levels->setText("");
    } else {
        foreach (entry_level level, l_entry_levels) {
            QString s_level = QString::number(level.n_level);

//            Если помещения в текущем уровне доступа полность совпадают с выделенными (без лишних)
//            то это нужный уровень доступа
            if (level.sl_accessed_rooms == slm_selected_rooms->stringList()) {
                s_calc_level = s_level;
//            В другом случае нужно проверить, является ли текущий уровень доступа возможным
            } else {
                QSet <QString>set_accessed_rooms = level.sl_accessed_rooms.toSet();
                QSet <QString>set_selected_rooms = slm_selected_rooms->stringList().toSet();

//                Если список помещений уровня содержит все выбранные помещения, то добавляем его
//                к списку возможных уровней и дополняем соответствующую строку
                if (set_accessed_rooms.contains(set_selected_rooms)) {
                    l_possible_levels.append(level);
                }
            }
        }

        struct sort_levels {
            bool operator() (entry_level el1, entry_level el2)
            {
                bool result;
                int len1 = el1.sl_accessed_rooms.length();
                int len2 = el2.sl_accessed_rooms.length();

                if (len1 == len2) {
                    result = el1.n_level < el2.n_level;
                } else {
                    result = len1 < len2;
                }

                return result;
            }
        } sort_entry_levels;

        std::sort(l_possible_levels.begin(), l_possible_levels.end(), sort_entry_levels);

        QString s_tmp;
        for(entry_level level : l_possible_levels) {
            QSet <QString>set_accessed_rooms = level.sl_accessed_rooms.toSet();
            QSet <QString>set_selected_rooms = slm_selected_rooms->stringList().toSet();
            QSet <QString>set_additional_rooms = set_accessed_rooms - set_selected_rooms;

            s_tmp = QString::number(level.n_level) + " (+ ";
            QSet<QString>::const_iterator iter = set_additional_rooms.constBegin();

//            Пробегаемся по дополнительным комнатам до предпоследней
            while (iter != (set_additional_rooms.constEnd() - 1)) {
                s_tmp += *iter + ", ";
                ++iter;
            }
//            Обрабатываем последнюю комнату
            s_tmp += *iter + ")";

            if (s_poss_levels.isEmpty()) {
                s_poss_levels = s_tmp;
            } else {
                s_poss_levels += "\n" + s_tmp;
            }
        }

        bool isCalcEmpty = s_calc_level.isEmpty();
        ui->btn_create_entry_level->setEnabled(isCalcEmpty);

        if (isCalcEmpty) {
            s_calc_level = "<b>УРОВЕНЬ ДОСТУПА НЕ СУЩЕСТВУЕТ</b>";
            ui->lbl_calculated_level->setStyleSheet("QLabel {color: red}");
        } else {
            ui->lbl_calculated_level->setStyleSheet("QLabel {color: black}");
        }

        ui->lbl_calculated_level->setText(s_calc_level);
        ui->lbl_possible_levels->setText(s_poss_levels);
    }
}

void MainWindow::on_lv_all_rooms_doubleClicked(const QModelIndex &index)
{
    QString s_room = index.data().toString();
    int n_room_id = slm_rooms->stringList().indexOf(s_room);

//    Убираем выбранное помещение из списка всех помещений
    slm_rooms->removeRow(n_room_id);

//    Добавляем помещение в список выбранных и сортируем его [список]
    sl_selected_rooms = slm_selected_rooms->stringList();
    sl_selected_rooms.append(s_room);
    sl_selected_rooms.sort();
    slm_selected_rooms->setStringList(sl_selected_rooms);

//    После этого заново рассчитываем уровни доступа
    this->update_levels();
}

void MainWindow::on_lv_selected_rooms_doubleClicked(const QModelIndex &index)
{
    QString s_room = index.data().toString();
    int n_room_id = slm_selected_rooms->stringList().indexOf(s_room);

//    Убираем выбранное помещение из списка всех помещений
    slm_selected_rooms->removeRow(n_room_id);

//    Добавляем помещение в список выбранных и сортируем его [список]
    sl_rooms = slm_rooms->stringList();
    sl_rooms.append(s_room);
    sl_rooms.sort();
    slm_rooms->setStringList(sl_rooms);

//    После этого заново рассчитываем уровни доступа
    this->update_levels();
}

void MainWindow::on_btn_select_room_clicked()
{
    QModelIndexList lst = ui->lv_all_rooms->selectionModel()->selectedIndexes();
    QStringList sl_mov_rooms;

    sl_rooms = slm_rooms->stringList();

    for (QModelIndex i : lst) {
        QString s_cur = i.data().toString();
        if (sl_rooms.contains(s_cur)) {
            sl_rooms.removeOne(s_cur);
            sl_mov_rooms.append(s_cur);
        }
    }

    slm_rooms->setStringList(sl_rooms);
    sl_mov_rooms += slm_selected_rooms->stringList();
    sl_mov_rooms.sort();
    slm_selected_rooms->setStringList(sl_mov_rooms);

    this->update_levels();
}

void MainWindow::on_btn_deselect_room_clicked()
{
    QModelIndexList lst = ui->lv_selected_rooms->selectionModel()->selectedIndexes();
    QStringList sl_mov_rooms;

    sl_selected_rooms = slm_selected_rooms->stringList();

    for (QModelIndex i : lst) {
        QString s_cur = i.data().toString();
        if (sl_selected_rooms.contains(s_cur)) {
            sl_selected_rooms.removeOne(s_cur);
            sl_mov_rooms.append(s_cur);
        }
    }

    slm_selected_rooms->setStringList(sl_selected_rooms);
    sl_mov_rooms += slm_rooms->stringList();
    sl_mov_rooms.sort();
    slm_rooms->setStringList(sl_mov_rooms);

    this->update_levels();
}

void MainWindow::on_btn_apply_level_clicked()
{
    int n_selected_level = ui->sb_predefined_level->value();

    bool res = entry_levels->valueInColumnExists(1, n_selected_level);
    if (!res) {
        QMessageBox msgBox(QMessageBox::Critical,
                           "Не существует уровень доступа",
                           "Заданный уровень доступа не существует",
                           QMessageBox::Ok);
        msgBox.exec();
        return;
    }

    entry_level el_cur_level;

    for (entry_level i : l_entry_levels) {
        if (i.n_level == n_selected_level) {
            el_cur_level = i;
            break;
        }
    }

    ui->lv_selected_rooms->selectAll();
    this->on_btn_deselect_room_clicked();

    QStringList sl_rooms = slm_rooms->stringList();\
    QStringList sl_selected_rooms;

    for (QString s_room : el_cur_level.sl_accessed_rooms) {
        if (sl_rooms.contains(s_room)) {
            sl_rooms.removeOne(s_room);
            sl_selected_rooms.append(s_room);
        }
    }

    slm_rooms->setStringList(sl_rooms);
    slm_selected_rooms->setStringList(sl_selected_rooms);

    this->update_levels();
}

void MainWindow::on_btn_create_entry_level_clicked()
{
    int n_new_row = entry_levels->rowsCount + 1;
    int n_new_level = entry_levels->cellAt(n_new_row - 1, 1)->value().toUInt() + 1;
    int n_rooms_count = entry_levels->colsCount;
    entry_level el_new_entry_level;

    el_new_entry_level.n_level = n_new_level;

    entry_levels->write(n_new_row, 1, n_new_level);

    sl_selected_rooms = slm_selected_rooms->stringList();

    for (int col = 2; col <= n_rooms_count; ++col) {
        QString s_cur_room = entry_levels->cellAt(1, col)->value().toString().trimmed();

        if (sl_selected_rooms.contains(s_cur_room)) {
            entry_levels->write(n_new_row, col, "х");

            el_new_entry_level.sl_accessed_rooms.append(s_cur_room);
            el_new_entry_level.sl_accessed_rooms.sort();
        }
    }

    l_entry_levels.append(el_new_entry_level);
    ++entry_levels->rowsCount;

    this->update_levels();

    entry_levels->save();

    QString msgText = "<b>Добавлен новый уровень доступа №" + QString::number(n_new_level) + ". Добавьте информацию в АБД!</b>";
    QString msgInf = "Двери:\n";

    foreach (QString room, el_new_entry_level.sl_accessed_rooms) {
        msgInf.append(room + "\n");
    }

    DoomDefenseDialog *msgBox = new DoomDefenseDialog(msgText, msgInf, this);

    msgBox->setWindowModality(Qt::WindowModal);

    msgBox->exec();
}

void MainWindow::apply_level()
{
    ui->btn_apply_level->click();
}

bool MainWindow::eventFilter(QObject *obj, QEvent *event)
{
    if (event->type() == QEvent::KeyPress) {
        QKeyEvent *keyEvent = static_cast<QKeyEvent *>(event);

        int key = keyEvent->key();

        if (key == Qt::Key_Return || key == Qt::Key_Enter) {
            bool res = entry_levels->valueInColumnExists(1, QVariant(ui->sb_predefined_level->value()));
            if (!res) {
                QMessageBox msgBox(QMessageBox::Critical,
                                   "Не существует уровень доступа",
                                   "Заданный уровень доступа не существует",
                                   QMessageBox::Ok);
                msgBox.exec();
            } else {
                ui->btn_apply_level->click();
            }
            return true;
        }
        else {
            return QObject::eventFilter(obj, event);
        }
    } else {
        // standard event processing
        return QObject::eventFilter(obj, event);
    }
}
