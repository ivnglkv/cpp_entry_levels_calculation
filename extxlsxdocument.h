#ifndef EXTXLSXDOCUMENT_H
#define EXTXLSXDOCUMENT_H

#include <QString>
#include <QVariant>
#include <QDebug>
#include "xlsxdocument.h"

using namespace QXlsx;

enum RowAction {
    Move,
    Remove,
    Insert
};

class ExtXlsxDocument
{
public:
    ExtXlsxDocument(const QString &file_name);

    void save();

    void moveRow(const int &src_row, const int &dst_row);
    void removeRow(const int &row);

    Cell *cellAt(const int &row, const int &col);

    void write(const int &row, const int &col, const QVariant &data, const Format &format=Format());

    bool valueInColumnExists(const int &col, const QVariant &val);

    int colsCount;
    int rowsCount;

private:
    QXlsx::Document *document;

    void recalculateDimensions();
};

#endif // EXTXLSXDOCUMENT_H
