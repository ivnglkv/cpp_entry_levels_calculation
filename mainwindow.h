#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QDebug>
#include <QFileDialog>
#include <QString>
#include <xlsxdocument.h>
#include <QStringList>
#include <QStringListModel>
#include <QList>
#include <QSet>
#include <QSettings>
#include <QFileInfo>
#include <QMessageBox>
#include <QPushButton>
#include <algorithm>
#include <QKeyEvent>
#include "extxlsxdocument.h"
#include "doomdefensedialog.h"

namespace Ui {
class MainWindow;
}

struct entry_level {
    int n_level;
    QStringList sl_accessed_rooms;

    bool operator ==(const entry_level &x) {
        if (x.sl_accessed_rooms.isEmpty())
            return false;
        if (sl_accessed_rooms == x.sl_accessed_rooms)
            return true;
        else
            return false;
    }

    bool operator >(const entry_level &x) {
        if (n_level > x.n_level) {
            return true;
        } else {
            return false;
        }
    }
};

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

    QStringList sl_rooms;
    QStringList sl_selected_rooms;

    QStringListModel *slm_rooms;
    QStringListModel *slm_selected_rooms;

    QList<entry_level> l_entry_levels;

    void update_levels();

private:
    Ui::MainWindow *ui;
    QSettings *p_settings;

//    QXlsx::Document *entry_levels;

    ExtXlsxDocument *entry_levels;

    void init_signals();

protected:
    bool eventFilter(QObject *obj, QEvent *event);

public slots:
    void choose_file();
    void load_data(QString s_file_name);
    void apply_level();
private slots:
    void on_lv_all_rooms_doubleClicked(const QModelIndex &index);
    void on_lv_selected_rooms_doubleClicked(const QModelIndex &index);
    void on_btn_select_room_clicked();
    void on_btn_deselect_room_clicked();
    void on_btn_apply_level_clicked();
    void on_btn_create_entry_level_clicked();
};

#endif // MAINWINDOW_H
