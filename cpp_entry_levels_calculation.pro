#-------------------------------------------------
#
# Project created by QtCreator 2016-02-10T15:40:14
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = cpp_entry_levels_calculation
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    extxlsxdocument.cpp \
    doomdefensedialog.cpp

HEADERS  += mainwindow.h \
    extxlsxdocument.h \
    doomdefensedialog.h

FORMS    += mainwindow.ui \
    doomdefensedialog.ui

OTHER_FILES += \
    .gitignore

CONFIG += c++11

include(3rdparty/qtxlsx/src/xlsx/qtxlsx.pri)
