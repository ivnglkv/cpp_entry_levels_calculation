#include "extxlsxdocument.h"

ExtXlsxDocument::ExtXlsxDocument(const QString &file_name)
{
    document = new Document(file_name);

    colsCount = document->dimension().columnCount();
    rowsCount = document->dimension().rowCount();

    this->recalculateDimensions();
}

void ExtXlsxDocument::save()
{
    document->save();
}

void ExtXlsxDocument::moveRow(const int &src_row, const int &dst_row)
{
    if (src_row == dst_row)
        return;

    Cell *src_cell;
    Format src_format;

    for (int n_cur_col = 1; n_cur_col <= colsCount; ++n_cur_col) {
        src_cell = this->cellAt(src_row, n_cur_col);

//        Если исходная ячейка пуста, то гарантированно затираем значение в целевой ячейке и уходим на следующую итерацию
        if (!src_cell) {
            this->write(dst_row, n_cur_col, "");
            continue;
        }

        src_format = src_cell->format();
        this->write(dst_row, n_cur_col, src_cell->value(), src_format);

//        Затираем значение в исходной ячейке
        this->write(src_row, n_cur_col, "");
    }

    if (src_row == this->rowsCount && dst_row < this->rowsCount)
        --this->rowsCount;
    else if (dst_row > this->rowsCount)
        ++this->rowsCount;

    return;
}

void ExtXlsxDocument::removeRow(const int &row)
{
    for (int n_cur_col = 1; n_cur_col <= colsCount; ++n_cur_col) {
        this->write(row, n_cur_col, "");
    }
    document->setRowFormat(row, Format(document->rowFormat(this->rowsCount+100)));

    if (row == this->rowsCount)
        --this->rowsCount;

    return;
}

void ExtXlsxDocument::recalculateDimensions()
{
    Cell *current_cell;
    QString data;

    this->rowsCount = 0;
    for (int i = 2; /* Мы не знаем, когда остановиться*/ ; ++i) {
        current_cell = document->cellAt(i, 1);

        if (!current_cell) {
            break;
        }

        data = current_cell->value().toString();

        if (data.isEmpty()) {
            break;
        }

        ++this->rowsCount;
    }
    ++this->rowsCount;

    this->colsCount = 0;
    for (int j = 2; ; ++j) {
        current_cell = document->cellAt(1, j);

        if (!current_cell) {
            break;
        }

        data = current_cell->value().toString();

        if (data.isEmpty()) {
            break;
        }

        ++this->colsCount;
    }
    ++this->colsCount;

    qDebug() << this->rowsCount << document->dimension().rowCount() << this->colsCount << document->dimension().columnCount();

    return;
}

Cell *ExtXlsxDocument::cellAt(const int &row, const int &col)
{
    return document->cellAt(row, col);
}

void ExtXlsxDocument::write(const int &row, const int &col, const QVariant &data, const Format &format)
{
    document->write(row, col, data, format);

    return;
}

bool ExtXlsxDocument::valueInColumnExists(const int &col, const QVariant &val)
{
    Cell *current_cell;
    QVariant data;
    bool found = false;

    for (int i = 1; i <= this->rowsCount; ++i) {
        current_cell = document->cellAt(i, col);

        if (!current_cell) {
            continue;
        }

        data = current_cell->value();

        if (data == val) {
            found = true;
            break;
        }
    }

    return found;
}
